import unittest as ut
import argparse as ap
from base64 import b64encode
import json

from pypsm import PSModule, PSFunction, PSArgument, pstypes

class Test_PSModule(ut.TestCase):
    def setUp(self):
        self.args = ap.Namespace()
        self.args.invoke = ('Test-Func', b64encode(json.dumps({
            'param1': 'Hello World',
            'param2': 20
        }).encode()))

    def test_normal(self):
        mod = PSModule(__file__)
        test_func = lambda param1, param2, param3: print(param1, param2, param3)
        mod.addfunc(PSFunction(
            'Test-Func', test_func,
            PSArgument('param1', pstypes.psstring),
            PSArgument('param2', pstypes.psint),
            PSArgument('param3', pstypes.psint, default=3)))
        
        mod(args=self.args)

    def test_annotations(self):
        mod = PSModule(__file__)

        @mod.function
        def test_func(param1: pstypes.psstring,
                      param2: pstypes.psint,
                      param3: pstypes.psint=3):
            """Test function documentation"""
            print(param1, param2, param3)

        mod(args=self.args)
        self.assertEqual(mod._funcs['Test-Func'].doc, "Test function documentation")

if __name__ == '__main__':
    ut.main()
