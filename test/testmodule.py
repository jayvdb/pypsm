from pypsm import PSModule
from pypsm.pstypes import psstring

mod = PSModule(__file__)

@mod.function
def test_func(p1: psstring):
    """Print hello <p1> (p1 is the value you specified)"""
    print('Hello %s' % p1)

mod()
