from setuptools import setup
import os, sys


setup(
    name='pypsm',
    version='0.1.4',
    description='Write PowerShell modules with python',
    author='Philip Stoop',
    author_email='stoopphilip97@gmail.com',
    packages=['pypsm']
)
